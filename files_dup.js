var files_dup =
[
    [ "backend_task_week2.py", "backend__task__week2_8py.html", [
      [ "BackendTask", "classbackend__task__week2_1_1BackendTask.html", "classbackend__task__week2_1_1BackendTask" ]
    ] ],
    [ "backend_week1.py", "backend__week1_8py.html", [
      [ "Backend_Task", "classbackend__week1_1_1Backend__Task.html", "classbackend__week1_1_1Backend__Task" ]
    ] ],
    [ "elevator.py", "elevator_8py.html", "elevator_8py" ],
    [ "encoder_task_week2.py", "encoder__task__week2_8py.html", [
      [ "EncoderTask", "classencoder__task__week2_1_1EncoderTask.html", "classencoder__task__week2_1_1EncoderTask" ]
    ] ],
    [ "encoder_week2.py", "encoder__week2_8py.html", "encoder__week2_8py" ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "frontend_week1.py", "frontend__week1_8py.html", "frontend__week1_8py" ],
    [ "frontend_week2.py", "frontend__week2_8py.html", "frontend__week2_8py" ],
    [ "ledpattern.py", "ledpattern_8py.html", "ledpattern_8py" ],
    [ "main_week1.py", "main__week1_8py.html", "main__week1_8py" ],
    [ "main_week2.py", "main__week2_8py.html", "main__week2_8py" ],
    [ "simonsays.py", "simonsays_8py.html", "simonsays_8py" ],
    [ "timeout_def_week1.py", "timeout__def__week1_8py.html", "timeout__def__week1_8py" ]
];