var classbackend__task__week2_1_1BackendTask =
[
    [ "__init__", "classbackend__task__week2_1_1BackendTask.html#a442c739724c683b7571510fde42d9e07", null ],
    [ "process_cmd", "classbackend__task__week2_1_1BackendTask.html#ad478a78db4eb5c65fb81dc66e022c7f8", null ],
    [ "run", "classbackend__task__week2_1_1BackendTask.html#a9c7b11c645d7367e32bffc4f4811cf29", null ],
    [ "transmit_arr", "classbackend__task__week2_1_1BackendTask.html#af7e5f47b4929e17b3f3c7d7318a520f9", null ],
    [ "try_get_cmd", "classbackend__task__week2_1_1BackendTask.html#a3ec05d6b8662264802885ceec58af18d", null ],
    [ "cmd", "classbackend__task__week2_1_1BackendTask.html#a9a34a4e93ac52abe1ebfad5e1dc8a865", null ],
    [ "data", "classbackend__task__week2_1_1BackendTask.html#a2ca4f96f4adce3af93d6a4ebb6cc1c5e", null ],
    [ "enc1", "classbackend__task__week2_1_1BackendTask.html#aaade7bb800c4bcf33514e85a01f2aa10", null ],
    [ "enc_task", "classbackend__task__week2_1_1BackendTask.html#a0cb63b9a780b449f2d47c9f36e3ce8b4", null ],
    [ "myuart", "classbackend__task__week2_1_1BackendTask.html#a30e3a81fd25c2c8b5ff83cb841f062af", null ],
    [ "state", "classbackend__task__week2_1_1BackendTask.html#a58b22b2a9b14be230c3013152700b6db", null ],
    [ "ticks_t0", "classbackend__task__week2_1_1BackendTask.html#a3b02ea02324d106c26f926d8e40a91a2", null ],
    [ "TIMEOUT_S", "classbackend__task__week2_1_1BackendTask.html#aa988024b1678f65adbe9309261ba2ff7", null ]
];